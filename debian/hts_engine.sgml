<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@


	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Koichi</firstname>">
  <!ENTITY dhsurname   "<surname>Akabe</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>January 16, 2013</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>vbkaisetsu@gmail.com</email>">
  <!ENTITY dhusername  "Koichi Akabe">
  <!ENTITY dhucpackage "<refentrytitle>htsengine</refentrytitle>">
  <!ENTITY dhpackage   "hts_engine">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2011-2013</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>HMM-based speech synthesis engine</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg><option>options</option></arg>
      <arg><option>infile</option></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>&dhpackage;</command> command.</para>

    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.
      Instead, it has documentation in the &gnu;
      <application>Info</application> format; see below.</para>

    <para><command>&dhpackage;</command> is a program that synthesize
      speech waveform from HMMs trained by the HMM-based speech synthesis
      system (HTS).</para>

  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <para>A summary of options is included below.
    </para>

    <variablelist>
      <varlistentry>
        <term><option>-m  htsvoice</option></term>
        <listitem><para>HTS voice files</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-od s</option></term>
        <listitem><para>filename of output label with duration</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-om s</option></term>
        <listitem><para>filename of output spectrum</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-of s</option></term>
        <listitem><para>filename of output Log F0</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-ol s</option></term>
        <listitem><para>filename of output low-pass filter</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-or s</option></term>
        <listitem><para>filename of output raw audio (generated speech)</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-ow s</option></term>
        <listitem><para>filename of output wav audio (generated speech)</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-ot s</option></term>
        <listitem><para>filename of output trace information</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-vp</option></term>
        <listitem><para>use phoneme alignment for duration</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-i  i f1 .. fi</option></term>
        <listitem><para>enable interpolation & specify number(i),coefficient(f)</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-s  i</option></term>
        <listitem><para>sampling frequency [auto][  1--   ]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-p  i</option></term>
        <listitem><para>frame period (point) [auto][  1--   ]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-a  f</option></term>
        <listitem><para>all-pass constant [auto][0.0--1.0]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-b  f</option></term>
        <listitem><para>postfiltering coefficient [0.0][0.0--1.0]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-r  f</option></term>
        <listitem><para>speech speed rate [1.0][0.0--   ]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-fm f</option></term>
        <listitem><para>add half-tone [0.0][   --   ]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-u  f</option></term>
        <listitem><para>voiced/unvoiced threshold[0.5][0.0--1.0]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-jm f</option></term>
        <listitem><para>weight of GV for spectrum [1.0][0.0--   ]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-jf f</option></term>
        <listitem><para>weight of GV for Log F0 [1.0][0.0--   ]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-z  i</option></term>
        <listitem><para>audio buffer size (if i==0, turn off) [  0][0--   ]</para></listitem>
      </varlistentry>
      <varlistentry>
        <term><option>infile</option></term>
        <listitem><para>label file</para></listitem>
      </varlistentry>
    </variablelist>
    <para>generated spectrum, log F0, and low-pass filter coefficient
      sequences are saved in natural endian, binary (float) format.
    </para>
  </refsect1>
  <refsect1>
    <title>EXAMPLE</title>

    <para>If you installed hts-voice-nitech-jp-atr503-m001 in the current
      directory, the following command let you make a voice file from
      input.lab:
      <blockquote>
        <programlisting>% hts_engine -s 48000 -p 240 -a 0.55 \\
          -m nitech_jp_atr503_m001.htsvoice \\
          -ow output.wav \\
          input.lab
        </programlisting>
      </blockquote>
    </para>
  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for
      the &debian; system (and may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the &gnu; General Public License, Version 2 any
      later version published by the Free Software Foundation.
    </para>
    <para>
      On Debian systems, the complete text of the GNU General Public
      License can be found in /usr/share/common-licenses/GPL.
    </para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
